import * as bitcoin from 'bitcoinjs-lib'
import {Buffer} from 'buffer'

import {pbkdf2Sync} from 'pbkdf2'
import {createDecipheriv as createDecipherivAes, createCipheriv as createCipherivAes} from 'browserify-aes'
import * as createHmac from 'create-hmac'

import ecc from './ecc-adapter'

import ECPairFactory from 'ecpair'
const ECPair = ECPairFactory(ecc)

import BIP32Factory from 'bip32'
const bip32 = BIP32Factory(ecc)

import {feeEstimates} from './esplora'
import {markInvalid, markValid, networkFromAddress, parseAddress} from './utils.js'

const estimatedTxVb = 186
const targetConfirmationBlocks = 3
const minimumRelayFee = 200

// OP_2 <server_pubkey> <seller_pubkey> <buyer_pubkey> OP_3 OP_CHECKMULTISIG
const twoOfThreeWitness = /^5221([a-f0-9]{66})21([a-f0-9]{66})21([a-f0-9]{66})53ae$/

function decryptSeed(encryptedSeed, password) {
  try {
    const esParts = encryptedSeed.split(":")
    if (esParts.length !== 5) { return null }

    const esVersion = esParts[0]
    if (esVersion !== 'ES1') { return null }

    const ciphertextHex = esParts[1]
    const ciphertext = Buffer.from(ciphertextHex, 'hex')
    const saltHex = esParts[2]
    const salt = Buffer.from(saltHex, 'hex')

    const kdfName = esParts[3]
    if (kdfName !== 'pbkdf2') { return null }

    const kdfIterations = parseInt(esParts[4])
    if (kdfIterations !== 10000) { return null }

    const aesKey = pbkdf2Sync(password, salt, kdfIterations, 16, 'sha256')
    // console.log("aesKey", aesKey.toString('hex'))

    const iv = ciphertext.subarray(0, 12)
    const authTag = ciphertext.subarray(-16)
    const content = ciphertext.subarray(12, -16) // contains auth tag at the end (16 bytes)

    const decipher = createDecipherivAes('aes-128-gcm', aesKey, iv, {authTagLength: 16})
    decipher.setAuthTag(authTag)
    const seed = decipher.update(content)
    decipher.final()

    const hmac = createHmac('sha256', Buffer.from(password))
    hmac.update(seed)
    const mac = hmac.digest().subarray(0, 16)
    if (Buffer.compare(mac, salt) !== 0) {
      console.log("Seed decryption failed: mac doesn't match salt")
      return null
    }
    return seed.toString('hex')
  } catch(e) {
    console.log("Seed decryption failed", e)
    return null;
  }
}

function encryptSeed(seedHex, password) {
  const esVersion = 'ES1'
  const kdfIterations = 10000
  const seed = Buffer.from(seedHex, 'hex')

  let hmac = createHmac('sha256', Buffer.from(password))
  hmac.update(seed)
  const salt = hmac.digest().subarray(0, 16)

  const aesKey = pbkdf2Sync(password, salt, kdfIterations, 16, 'sha256')

  hmac = createHmac('sha256', aesKey)
  hmac.update(seed)
  const iv = hmac.digest().subarray(0, 12)

  const cipher = createCipherivAes('aes-128-gcm', aesKey, iv, {authTagLength: 16})
  let ciphertext = cipher.update(seed)
  cipher.final()

  return `${esVersion}:${iv.toString('hex')}${ciphertext.toString('hex')}${cipher.getAuthTag().toString('hex')}:${salt.toString('hex')}:pbkdf2:${kdfIterations}`
}

function derivePrivateKey(seed, index) {
  return bip32.
    fromSeed(Buffer.from(seed, 'hex')).
    derivePath(index.toString()).
    privateKey.
    toString('hex')
}

function validateAddressInput(input, validNetwork) {
  const address = input.value
  const addressNetwork = input.parentElement.getElementsByClassName('addressNetwork')[0]
  const {address: btcAddress, network} = parseAddress(address)
  if (btcAddress === null || network != validNetwork) {
    markInvalid(input)
    return false
  }

  addressNetwork.innerText = network
  markValid(input)
  return true
}

function validateNumericInput(input) {
  const i = Number(input.value)
  if (isNaN(i) || i.toString() != input.value) {
    markInvalid(input)
    return false
  }

  markValid(input)
  return i
}

function parseWitnessScript(script) {
  const result = script.match(twoOfThreeWitness)
  if (!result) {
    return null
  }
  return result.slice(1)
}

function validateWitnessScript(scriptInput, address) {
  const script = scriptInput.value
  const publicKeys = parseWitnessScript(script)
  if (!publicKeys) {
    markInvalid(scriptInput)
    return false
  }

  const escrowNetwork = networkFromAddress(address)
  const correctAddress = bitcoin.payments.p2sh({
    redeem: bitcoin.payments.p2wsh({
      redeem: bitcoin.payments.p2ms({
        m: 2,
        pubkeys: publicKeys.map((x) => Buffer.from(x, 'hex')),
        network: escrowNetwork == 'mainnet' ? bitcoin.networks.bitcoin : bitcoin.networks.testnet
      })
    })
  }).address
  if (address != correctAddress) {
    console.log(`Address should've been ${correctAddress}, but was ${address}`)
    markInvalid(scriptInput)
    return false
  }

  markValid(scriptInput)
  return publicKeys
}

async function recommendedFee(network) {
  const result = await feeEstimates(network)
  const variableFee = result[targetConfirmationBlocks.toString()] * estimatedTxVb
  return Math.max(variableFee, minimumRelayFee)
}

function buildSkeletonTx({escrowAddress, utxos, outputs, witnessScript}) {
  const publicKeys = parseWitnessScript(witnessScript)

  const network = networkFromAddress(escrowAddress) == 'mainnet' ?
    bitcoin.networks.bitcoin : bitcoin.networks.testnet
  const psbt = new bitcoin.Psbt({network})
  const payment = bitcoin.payments.p2sh({
    redeem: bitcoin.payments.p2wsh({
      redeem: bitcoin.payments.p2ms({
        m: 2,
        pubkeys: publicKeys.map((x) => Buffer.from(x, 'hex')),
        network: network
      })
    })
  })

  utxos.forEach((utxo) => {
    psbt.addInput({
      hash: utxo.txid,
      index: utxo.vout,
      value: utxo.value,
      witnessUtxo: {
        script: Buffer.from(utxo.scriptPubKey, 'hex'),
        value: utxo.value
      },
      witnessScript: payment.redeem.redeem.output,
      redeemScript: payment.redeem.output
    })
  })

  outputs.forEach((output) => {
    psbt.addOutput(output)
  })

  return psbt.toHex()
}

const buildPsbt = ({psbtHex, key, network}) => {
  const psbt = bitcoin.Psbt.fromHex(psbtHex, {network})
  const signer = ECPair.fromPrivateKey(Buffer.from(key, 'hex'))
  psbt.signAllInputs(signer)
  return psbt.toHex()
}

const buildTx = ({psbtHex, network}) => {
  const psbt = bitcoin.Psbt.fromHex(psbtHex, {network})
  psbt.finalizeAllInputs()
  return psbt.extractTransaction().toHex()
}

export {
  buildSkeletonTx, buildPsbt, buildTx,
  derivePrivateKey, decryptSeed, encryptSeed,
  recommendedFee,
  validateAddressInput, validateNumericInput, validateWitnessScript
}

export {broadcast, unspents, txLink} from './esplora'
