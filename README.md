# Escrow extraction tool

A single-page application to run the transaction for extracting funds from escrow.

## Developing and building

```
# Start web-server
yarn install
yarn start
```

```
# Build
yarn build
```

## Usage

Open `dist/index.html` in a browser.

Prepare to enter:

* *encrypted seed*
* *payment password*
* *derivation index*
* *escrow address*
* *witness script*

Collaborate with your counterparty (the holder of the second key to the escrow) and split the roles. One of you will be a **transaction creator** and the other a **broadcaster**. Then follow the corresponding workflow.

## Transaction creator

Select desired blockchain network at the top.

1. Fill in ①: *encrypted seed*, *payment password*, *derivation index*. Ensure the *private key* appears.
2. Fill in ②:
    - *escrow address*, and wait for *UTXOs (JSON)* to appear (which will be fetched automatically from a blockchain explorer).
    - *witness script*, ensure it is validated as correct (green hint at the bottom of the witness script input appears).
3. Fill in ③:
     - Enter desired *output address 1*, *value 1* (in satoshis), *output address 2*, *value 2* (in satoshis). Agree with the other party on outputs and values beforehand. Typically, the first output is your own address, and the second output is the other party's address.
    - Check that appeared values: *Goes to outputs*, *Network fee*, *Recommended network fee*, are valid and correct. **Note that any funds that don't go to outputs will go to miners' fees**. Increase or decrease outputs' values accordingly so that *Network fee* is equal to *Recommended fee*.
    - Ensure *skeleton TX hex* appears.
4. Copy from ④:
    - Copy *Partially signed TX* and send it to the broadcaster.

## Broadcaster

Select desired blockchain network at the top.

1. Get a partially signed TX (hex string) from the transaction creator.
2. Fill in ①: *encrypted seed*, *payment password*, *derivation index*. Ensure the *private key* appears.
3. Fill in ③: paste received hex string into *Skeleton TX hex*. Leave everything else in this section blank.
4. Validate ④:
 - Ensure *TX* appears.
 - **Independently validate TX** (do not skip this step)
    - Open the transaction decoder, for example: https://live.blockcypher.com/btc/decodetx/
    - Select the blockchain network at the bottom ("Bitcoin" for mainnet, "Bitcoin testnet" for testnet)
    - Paste transaction hex to the corresponding field
    - Click "Decode transaction"
    - Check that input is the escrow address and that output addresses and values are the same as you agreed upon with your counterparty
 - Click "Broadcast" and ensure the transaction was broadcasted (a green hint appears below the button, and the transaction is now displayed in a blockchain explorer or wallet).
